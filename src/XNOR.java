/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author CJA
 */
public class XNOR extends Bauteil{
    XNOR(int id){
        super();
        this.id = id;
        setBounds(this.btnX + ((this.id % 7) * this.btnShift), this.btnY + ((this.id % 7) * this.btnShift), this.btnWidtht, this.btnHeight);
        setText("XNOR " + this.id);
        System.out.println(id);
        updateState();
    }
    public void updateState(){
        int countTrue = 0;
        for(int i = 0; i < inp.size(); i++){
            if(inp.get(i).state == true) countTrue++;
        }
        if(countTrue == 1) this.state = false;
        else this.state = true;
        updateColor();   
        updateFollower();
    }
}
