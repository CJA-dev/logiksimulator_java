/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 07.08.2015
  * @author Michael Lange
  */


import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;

public class SimulationPanel extends JPanel { 
  
  // Anfang Attribute
  private Image image;
  private Graphics2D g2d;
  
  //! var to store conected bt
  public Bauteil conctBt[] = new Bauteil[2];
  private Point btnLatestXY = new Point(100, 100);
    
  public SimulationPanel() {                                                    //Konstruktor setzt bevorzugte Gre, legt das Layout fest
    super();
    this.setPreferredSize(new Dimension(1000, 1000));
    this.setLayout(null);
  }
  
  // Anfang Methoden
  @Override
  public Dimension getPreferredSize() {
    Dimension size = super.getPreferredSize();
    if (image != null) {
      size.width = image.getWidth(this);
      size.height = image.getHeight(this);
    }
    return size;
  }
  
  public void setPaintColor(final Color color) {
    g2d.setColor(color);
  }
  
  public void clearPaint() {
    g2d.setBackground(Color.WHITE);
    g2d.clearRect(0, 0, getWidth(), getHeight());
    repaint();
    g2d.setColor(Color.BLACK);
  }
  
  @Override
  public void paintComponent(final Graphics g) {
    super.paintComponent(g);
    
    if (image == null || image.getWidth(this) < getSize().width                 //Image initialisieren (ging vorher nicht) oder an Gre des Simulatorpanels anpassen
    || image.getHeight(this) < getSize().height) {
      resetImage();
    } // end of if
    Graphics2D g2 = (Graphics2D) g;                                             //Erst jetzt kann man auch den Grafikkontext erhalten
    Rectangle r = g.getClipBounds();
    this.updateImage();
    g2.drawImage(image, r.x, r.y, r.width + r.x, r.height + r.y,
    r.x, r.y, r.width + r.x, r.height + r.y, this);
  }
  
  public void resetImage() {                                                    
    Image saveImage = image;                                                    //Verhindert das Flackern (so gut es geht)
    Graphics2D saveG2d = g2d;
    image = createImage(getWidth(), getHeight());
    g2d = (Graphics2D) image.getGraphics();
    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
    RenderingHints.VALUE_ANTIALIAS_ON);
    g2d.setBackground(Color.WHITE);
    g2d.clearRect(0, 0, getWidth(), getHeight());
    g2d.setColor(Color.BLACK);
    g2d.setStroke(new BasicStroke(2));

    if (saveG2d != null) {
      g2d.setColor(saveG2d.getColor());
      g2d.drawImage(saveImage, 0, 0, this);
      saveG2d.dispose();
    } // end of if
  }
  
  public Graphics2D getG2d() {
    return g2d;
  }
  
  public void updateImage() {                                                   //Diese Methode soll in das Image die Verbindungen zwischen den Bauteilen
    clearPaint();                                                               //als Strecken zeichnen. Die eigene paint-Methode wird zu oft aufgerufen, als
  }

  public void deleteBtn(){ //* delete Button 
    int deleteID = 0;
    do{
      String inp = JOptionPane.showInputDialog("Lösche Bauteil.\nBauteil ID:");  //* ask for bt to delete
      try{
        deleteID = Integer.parseInt(inp);
      }catch(Exception e){
        deleteID = 0;
        System.out.println(e);
      }
    }while(deleteID < 1); //* repeat until a valid number is comitted; ID starts by 1 (not lika a real pc exprert)

    //* loop through every bt
    int countBtns = this.getComponentCount(); 
    for(int i = 0; i < countBtns; i++){
      Bauteil b = (Bauteil) this.getComponent(i);   //* get component of current bt
      if(deleteID == b.id){ //! if current bt comitted id == bt id
        // System.out.println("id: " + b.id);
        // System.out.println("i: " + i);

        //* delete bt in input list of following bts and run updateState of them
        // output lists doesn't matter -> no gate with limitted output
        for (Bauteil out : b.out) { // loop through output elements
          for(int k = 0; k < out.inp.size(); k++){  // search in input l ist of following bt for own id
            if(out.inp.get(k).id == deleteID){
              System.out.println(out.inp.get(k).id);
              out.inp.remove(k);  // delete
            }
          }
        }

        this.remove(i);
        this.updateImage();
      }
    }
  }
  
  // Ende Methoden
} // end of SimulationPanel
