/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author CJA
 */
public class NOR extends Bauteil{
    NOR(int id){
        super();
        this.id = id;
        setBounds(this.btnX + ((this.id % 7) * this.btnShift), this.btnY + ((this.id % 7) * this.btnShift), this.btnWidtht, this.btnHeight);
        setText("NOR " + this.id);
        System.out.println("Created NOR id:" + id);
        updateState();
    }
    public void updateState(){
        int countTrue = 0;
        for(int i = 0; i < inp.size(); i++){
            if(inp.get(i).state == false) countTrue++;
        }
        if(countTrue > 0) this.state = false;
        else this.state = true;
        updateColor();
        updateFollower();
    }
}
