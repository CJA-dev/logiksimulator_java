/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author CJA
 */
public class NOT extends Bauteil{
    NOT(int id){
        super();
        this.id = id;
        setBounds(this.btnX + ((this.id % 7) * this.btnShift), this.btnY + ((this.id % 7) * this.btnShift), this.btnWidtht, this.btnHeight);
        setText("NOT " + this.id);
        System.out.println("Created NOT id:" + id);
        updateState();
    }
    public void updateState(){
        if(inp.size() > 1) showMsg("An dieses Gatter kann nur ein Eingang angeschlossen werden. Bitte entfernen sie alle überflüssigen Eingänge.");
        else if(inp.size() == 1) this.state = !inp.get(0).state;  //* invert state
        System.out.println(this.state);
        // System.out.println(inp.get(0).state);
        updateColor();
        updateFollower();
    }
}
