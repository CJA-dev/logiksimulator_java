/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

public class LED extends Bauteil{
    LED(int id){
        super();
        this.id = id;
        setBounds(this.btnX + ((this.id % 7) * this.btnShift), this.btnY + ((this.id % 7) * this.btnShift), this.btnWidtht, this.btnHeight);
        setText("LED " + this.id);
        System.out.println(id);
        updateState();
    }
    public void updateState(){
        if(inp.size() > 1){
            System.out.println("Error; Eine LED kann nur einen Eingang haben.");
        }else if(inp.size() < 1){
            System.out.println("Error; Eine LED muss mindestens einen Eingang haben.");
        }else{
            boolean inpState = inp.get(0).state;
            this.state = inpState;
            updateColor();
        }
    }
}
